﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace SisVentas.Models
{
    public class SisVentasContext : DbContext
    {
        public SisVentasContext (DbContextOptions<SisVentasContext> options)
            : base(options)
        {
        }

        public DbSet<Categoria> Categoria { get; set; }


    }
}
