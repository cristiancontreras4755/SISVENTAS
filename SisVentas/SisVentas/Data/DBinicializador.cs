﻿using SisVentas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SisVentas.Data
{
    public class DBinicializador
    {

        public static void Initializer(SisVentasContext Context) {

            if (Context.Categoria.Any()) {
                return;
            }

           var Categorias = new Categoria[] {
               new Categoria{Nombre="PROGRAMACION",Descripcion="CURSO DE PROGRAMACION",Estado=true},
               new Categoria{Nombre="DISEÑO GRAFICO",Descripcion="CURSO DE DISEÑO GRAFICO",Estado=true}

              };

            foreach (var cat in Categorias)
            {

                Context.Categoria.Add(cat);
            }
            Context.SaveChanges(); 
        }




    }
}
