﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SisVentas.Models
{
    public class Categoria
    {

        public int CategoriaID { get; set; }
        [Required]
        [StringLength(50,MinimumLength =3,ErrorMessage ="el nombre debe tener  3 a50 caracteres")]
        public string Nombre { get; set; }
        [StringLength(250,ErrorMessage ="no debe exceder  a lo 256 carateres")]
        public string Descripcion { get; set; }
        public bool Estado { get; set; }


    }
}
